import "package:nodejs/nodejs.dart";
import "package:electron/electron_main.dart";

void main() {
  app.onWindowAllClosed.listen((_) {
    if (process.platform != "darwin") {
      app.quit();
    }
  });

  app.onReady.listen((_) {
    BrowserWindow win = new BrowserWindow();
    win.loadURL("file://$dirname/index.html");

    win.onClosed.listen((_) {
      win = null;
    });
  });
}
