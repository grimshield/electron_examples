import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:angular2/platform/browser.dart';

import 'package:electron_example/angular2/app_component.dart';
import 'package:angular2/platform/common.dart';

void main() {
  bootstrap(AppComponent, <dynamic>[
    ROUTER_PROVIDERS,
    const Provider(LocationStrategy, useClass: HashLocationStrategy)
  ]);
}
