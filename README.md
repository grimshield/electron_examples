# electron_example

As his name indicate this repos will provide some example of how to use electron.dart

# How to try the example

Install electron-prebuild with:
```sh
sudo npm install -g electron-prebuilt
```

Get the dependencies with :
```sh
pub get
```

Build the example with :
```sh
pub build example
```

Launch an example with :
```sh
electron build/example/hello_world
```

Have fun !
