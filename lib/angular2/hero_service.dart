import 'dart:async';

import 'package:angular2/core.dart';

import 'hero.dart';
import 'mock_heroes.dart';

@Injectable()
class HeroService {
  Future<List<Hero>> getHeroes() async => heroes;

  // See the "Take it slow" appendix
  Future<List<Hero>> getHeroesSlowly() {
    return new Future<List<Hero>>.delayed(
        const Duration(seconds: 2), () => heroes // 2 seconds
        );
  }

  Future<Hero> getHero(int id) async =>
      heroes.where((Hero hero) => hero.id == id).first;
}
