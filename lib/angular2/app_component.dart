import 'package:angular2/core.dart';
import 'package:angular2/router.dart';

import "package:nodejs/nodejs.dart";

import 'heroes_component.dart';
import 'hero_service.dart';
import 'dashboard_component.dart';
import 'hero_detail_component.dart';

@Component(
    selector: 'my-app',
    template: '''
      <h1>{{title}}</h1>
      <p>
        We are using node <span>{{nodeVersion}}</span>,<br/>
        Chrome <span>{{chromeVersion}}</span>,<br/>
        and Electron <span>{{electronVersion}}</span>.
    </p>
      <nav>
        <a [routerLink]="['Dashboard']">Dashboard</a>
        <a [routerLink]="['Heroes']">Heroes</a>
      </nav>
      <router-outlet></router-outlet>''',
    styleUrls: const <String>['app_component.css'],
    directives: const <dynamic>[ROUTER_DIRECTIVES],
    providers: const <dynamic>[HeroService])
@RouteConfig(const <Route>[
  const Route(
      path: '/dashboard',
      name: 'Dashboard',
      component: DashboardComponent,
      useAsDefault: true),
  const Route(
      path: '/detail/:id', name: 'HeroDetail', component: HeroDetailComponent),
  const Route(path: '/heroes', name: 'Heroes', component: HeroesComponent)
])
class AppComponent {
  String title = 'Electron Tour of Heroes';
  String nodeVersion = process.versions["node"];
  String chromeVersion = process.versions["chrome"];
  String electronVersion = process.versions["electron"];
}
